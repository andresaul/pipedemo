<?php

namespace Pipedrive\DemoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BxUser
 */
class BxUser
{
    /**
     * @var integer
     */
    private $user_id;

    /**
     * @var string
     */
    private $location;

    /**
     * @var integer
     */
    private $age;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $bookRatings;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->bookRatings = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get user_id
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return BxUser
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set age
     *
     * @param integer $age
     * @return BxUser
     */
    public function setAge($age)
    {
        $this->age = $age;

        return $this;
    }

    /**
     * Get age
     *
     * @return integer 
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Add bookRatings
     *
     * @param \Pipedrive\DemoBundle\Entity\BxBookRating $bookRatings
     * @return BxUser
     */
    public function addBookRating(\Pipedrive\DemoBundle\Entity\BxBookRating $bookRatings)
    {
        $this->bookRatings[] = $bookRatings;

        return $this;
    }

    /**
     * Remove bookRatings
     *
     * @param \Pipedrive\DemoBundle\Entity\BxBookRating $bookRatings
     */
    public function removeBookRating(\Pipedrive\DemoBundle\Entity\BxBookRating $bookRatings)
    {
        $this->bookRatings->removeElement($bookRatings);
    }

    /**
     * Get bookRatings
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBookRatings()
    {
        return $this->bookRatings;
    }
    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $state;

    /**
     * @var string
     */
    private $country;


    /**
     * Set city
     *
     * @param string $city
     * @return BxUser
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return BxUser
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return BxUser
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }
}
