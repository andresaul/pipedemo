<?php

namespace Pipedrive\DemoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BxBookRating
 */
class BxBookRating
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $bookRating;

    /**
     * @var \Pipedrive\DemoBundle\Entity\BxBook
     */
    private $book;

    /**
     * @var \Pipedrive\DemoBundle\Entity\BxUser
     */
    private $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bookRating
     *
     * @param integer $bookRating
     * @return BxBookRating
     */
    public function setBookRating($bookRating)
    {
        $this->bookRating = $bookRating;

        return $this;
    }

    /**
     * Get bookRating
     *
     * @return integer 
     */
    public function getBookRating()
    {
        return $this->bookRating;
    }

    /**
     * Set book
     *
     * @param \Pipedrive\DemoBundle\Entity\BxBook $book
     * @return BxBookRating
     */
    public function setBook(\Pipedrive\DemoBundle\Entity\BxBook $book = null)
    {
        $this->book = $book;

        return $this;
    }

    /**
     * Get book
     *
     * @return \Pipedrive\DemoBundle\Entity\BxBook 
     */
    public function getBook()
    {
        return $this->book;
    }

    /**
     * Set user
     *
     * @param \Pipedrive\DemoBundle\Entity\BxUser $user
     * @return BxBookRating
     */
    public function setUser(\Pipedrive\DemoBundle\Entity\BxUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Pipedrive\DemoBundle\Entity\BxUser 
     */
    public function getUser()
    {
        return $this->user;
    }
 
}
