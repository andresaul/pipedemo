<?php

namespace Pipedrive\DemoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BxBook
 */
class BxBook
{
    /**
     * @var string
     */
    private $isbn;

    /**
     * @var string
     */
    private $bookTitle;

    /**
     * @var string
     */
    private $bookAuthor;

    /**
     * @var integer
     */
    private $yearOfPublication;

    /**
     * @var string
     */
    private $publisher;

    /**
     * @var string
     */
    private $imageUrlS;

    /**
     * @var string
     */
    private $imageUrlM;

    /**
     * @var string
     */
    private $imageUrlL;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $bookRatings;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->bookRatings = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set isbn
     *
     * @param string $isbn
     * @return BxBook
     */
    public function setIsbn($isbn)
    {
        $this->isbn = $isbn;

        return $this;
    }

    /**
     * Get isbn
     *
     * @return string 
     */
    public function getIsbn()
    {
        return $this->isbn;
    }

    /**
     * Set bookTitle
     *
     * @param string $bookTitle
     * @return BxBook
     */
    public function setBookTitle($bookTitle)
    {
        $this->bookTitle = $bookTitle;

        return $this;
    }

    /**
     * Get bookTitle
     *
     * @return string 
     */
    public function getBookTitle()
    {
        return $this->bookTitle;
    }

    /**
     * Set bookAuthor
     *
     * @param string $bookAuthor
     * @return BxBook
     */
    public function setBookAuthor($bookAuthor)
    {
        $this->bookAuthor = $bookAuthor;

        return $this;
    }

    /**
     * Get bookAuthor
     *
     * @return string 
     */
    public function getBookAuthor()
    {
        return $this->bookAuthor;
    }

    /**
     * Set yearOfPublication
     *
     * @param integer $yearOfPublication
     * @return BxBook
     */
    public function setYearOfPublication($yearOfPublication)
    {
        $this->yearOfPublication = $yearOfPublication;

        return $this;
    }

    /**
     * Get yearOfPublication
     *
     * @return integer 
     */
    public function getYearOfPublication()
    {
        return $this->yearOfPublication;
    }

    /**
     * Set publisher
     *
     * @param string $publisher
     * @return BxBook
     */
    public function setPublisher($publisher)
    {
        $this->publisher = $publisher;

        return $this;
    }

    /**
     * Get publisher
     *
     * @return string 
     */
    public function getPublisher()
    {
        return $this->publisher;
    }

    /**
     * Set imageUrlS
     *
     * @param string $imageUrlS
     * @return BxBook
     */
    public function setImageUrlS($imageUrlS)
    {
        $this->imageUrlS = $imageUrlS;

        return $this;
    }

    /**
     * Get imageUrlS
     *
     * @return string 
     */
    public function getImageUrlS()
    {
        return $this->imageUrlS;
    }

    /**
     * Set imageUrlM
     *
     * @param string $imageUrlM
     * @return BxBook
     */
    public function setImageUrlM($imageUrlM)
    {
        $this->imageUrlM = $imageUrlM;

        return $this;
    }

    /**
     * Get imageUrlM
     *
     * @return string 
     */
    public function getImageUrlM()
    {
        return $this->imageUrlM;
    }

    /**
     * Set imageUrlL
     *
     * @param string $imageUrlL
     * @return BxBook
     */
    public function setImageUrlL($imageUrlL)
    {
        $this->imageUrlL = $imageUrlL;

        return $this;
    }

    /**
     * Get imageUrlL
     *
     * @return string 
     */
    public function getImageUrlL()
    {
        return $this->imageUrlL;
    }

    /**
     * Add bookRatings
     *
     * @param \Pipedrive\DemoBundle\Entity\BxBookRating $bookRatings
     * @return BxBook
     */
    public function addBookRating(\Pipedrive\DemoBundle\Entity\BxBookRating $bookRatings)
    {
        $this->bookRatings[] = $bookRatings;

        return $this;
    }

    /**
     * Remove bookRatings
     *
     * @param \Pipedrive\DemoBundle\Entity\BxBookRating $bookRatings
     */
    public function removeBookRating(\Pipedrive\DemoBundle\Entity\BxBookRating $bookRatings)
    {
        $this->bookRatings->removeElement($bookRatings);
    }

    /**
     * Get bookRatings
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBookRatings()
    {
        return $this->bookRatings;
    }
}
