<?php

namespace Pipedrive\DemoBundle\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\View\View AS FOSView;
use FOS\RestBundle\Util\Codes;

use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations;
use Symfony\Component\Validator\Constraints as Assert;
use Pipedrive\DemoBundle\Entity\BxUser;
use Pipedrive\DemoBundle\Form\BxUserType;

use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;

use Pipedrive\DemoBundle\Controller\PipeBaseController;


use Pipedrive\DemoBundle\Security\ApiKeyAuthenticator;
//use Pipedrive\DemoBundle\Security\SimpleApiTokenAuthenticator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Pipedrive\DemoBundle\Controller\TokenAuthenticatedController;

class UserRESTController extends PipeBaseController implements TokenAuthenticatedController
{

   /**
    *
    * @Annotations\QueryParam(name="city", nullable=true, description="City")
    * @Annotations\QueryParam(name="state", nullable=true, description="State")    
    * @Annotations\QueryParam(name="country", nullable=true, description="Country")    
    * @Annotations\QueryParam(name="age", nullable=true, description="Age")
    * @Annotations\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing pages.")
    * @Annotations\QueryParam(name="limit", requirements="\d+", default="5", description="How many pages to return.")
    *    
    *       
    * @ApiDoc(
    *  resource=true,
    *  description="Returns a collection of Users by filters",
    *  filters={
    *      {"name"="location", "dataType"="integer", "required"=false, "description"="Location"},
    *      {"name"="age", "dataType"="string", "required"=false, "description"="Age"},       
    *  },   
    *  ) 
    *
    * @param ParamFetcher $paramFetcher 
    * @return JsonResponse 
    * 
    */

    public function getUsersAction(ParamFetcher $paramFetcher)
    {

        $param = $paramFetcher->all();

        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('PipedriveDemoBundle:BxUser')->getByParam($param);

        return $users;
    }

   /**
    * Get single User by id
    *
    * @ApiDoc(
    *   resource=true,     
    *   description = "Gets a User for a given id",
    *   output={"class"="BxUser"},
    *   statusCodes = {
    *     200 = "Returned when successful",
    *     404 = "Returned when the page is not found"
    *   },
    *   parameter={
    *     {"name"="id", "dataType"="integer", "required"=true, "description"="Id"},
    *   },      
    * )
    *
    * @param Request $request the request object
    *
    * @return array
    *
    * @throws NotFoundHttpException when page not exist
    */

    public function getUserAction($id)
    {

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('PipedriveDemoBundle:BxUser')->getOneById($id);

        if(!$user) {
            throw $this->createNotFoundException('User not found');
        }

        return $user;
    }


   /**
    * Create a new User entity.
    * @ApiDoc(
    *  resource=true,
    *  description="Create a new User entity",
    *  output={"class"="BxUser"},
    *  formType="Pipedrive\DemoBundle\Form\BxUserType",
    *  parameters={
    *      {"name"="city", "dataType"="string", "required"=false, "description"="City"},
    *      {"name"="state", "dataType"="string", "required"=false, "description"="State"},
    *      {"name"="country", "dataType"="string", "required"=false, "description"="Country"},    
    *      {"name"="age", "dataType"="string", "required"=false, "description"="Age"},
    *  },
    *   statusCodes = {
    *     201 = "Returned when entity successful created",
    *     400 = "Returned when the form is not valid",
    *   },
    * )     
    *
    * @View(statusCode=201, serializerEnableMaxDepthChecks=true)
    *
    * @param Request $request
    *
    * @return Response
    *
    */

    public function postUserAction(Request $request)
    {
        $entity = new BxUser();
        $form = $this->createForm(new BxUserType(), $entity, array("method" => $request->getMethod()));
        $this->removeExtraFields($request, $form);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $entity;
        } 

        return FOSView::create(array('errors' => $form->getErrors()), Codes::HTTP_BAD_REQUEST);
    }


 /**
    * Update a User entity
    *
    * @ApiDoc(
    *  resource=true,
    *  description="Update a new User entity",
    *  output={"class"="BxUser"},
    *  formType="Pipedrive\DemoBundle\Form\BxUserType",
    *  parameters={
    *      {"name"="city", "dataType"="string", "required"=true, "description"="City"},
    *      {"name"="state", "dataType"="string", "required"=true, "description"="State"},
    *      {"name"="country", "dataType"="string", "required"=true, "description"="Country"},
    *      {"name"="age", "dataType"="string", "required"=false, "description"="Age"},
    *  }      
    * )     
    *
    * @View(statusCode=201, serializerEnableMaxDepthChecks=true)
    *
    * @param Request $request
    * @param string $isbn
    *
    * @return Response
    *
    */

    public function putUserAction(Request $request, $id)
    {
        try {
                
            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('PipedriveDemoBundle:BxUser')->findOneBy(array('user_id' => $id));

            if(!$user) {
                throw $this->createNotFoundException('User not found');
            }

            $request->setMethod('PATCH'); //Treat all PUTs as PATCH
            $form = $this->createForm(new BxUserType(), $user, array("method" => $request->getMethod()));
            $this->removeExtraFields($request, $form);
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->flush();

                return $user;
            }

            return FOSView::create(array('errors' => $form->getErrors()), Codes::HTTP_BAD_REQUEST);
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }    


   /**
    * Delete a User entity.
    *
    * @ApiDoc(
    *  resource=true,
    *  description="Remove User entity",   
    *  statusCodes = {
    *     204 = "Returned when entity successful removed",
    *   },
    * )      
    *
    * @View(statusCode=204)
    *
    * @param Request $request
    * @param $id
    *
    * @return Response
    */

    public function deleteUserAction(Request $request, $id)
    {
        try {
            $em   = $this->getDoctrine()->getManager();
            $user = $em->getRepository('PipedriveDemoBundle:BxUser')->find($id);

            if(!$user) {
                throw $this->createNotFoundException('User not found');
            }

            $em->remove($user);
            $em->flush();

            return null;
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }    
   
}
