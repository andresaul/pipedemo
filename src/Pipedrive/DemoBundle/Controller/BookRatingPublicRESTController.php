<?php

namespace Pipedrive\DemoBundle\Controller;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\View\View AS FOSView;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations;
use Symfony\Component\Validator\Constraints as Assert;

use FOS\RestBundle\Controller\FOSRestController;
use Pipedrive\DemoBundle\Controller\PipeBaseController;

class BookRatingPublicRESTController extends FOSRestController {

   /**
    *
    * @Annotations\QueryParam(name="country", nullable=false, description="Country")    
    * @Annotations\QueryParam(name="isbn", nullable=false, description="Isbn")        
    * @Annotations\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing pages.")
    * @Annotations\QueryParam(name="limit", requirements="\d+", default="5", description="How many pages to return.")
    *    
    *       
    * @ApiDoc(
    *  comment="fds",
    *  resource=true,
    *  description="Returns a collection of BookRatings by filters",
    *  filters={
    *      {"name"="country", "dataType"="integer", "required"=true, "description"="Location"},
    *      {"name"="isbn", "dataType"="string", "required"=true, "description"="Isbn"},    
    *  },   
    *  ) 
    *
    * @param ParamFetcher $paramFetcher 
    * @return JsonResponse 
    * 
    */

    public function getBooksRankingPerLocationAction(ParamFetcher $paramFetcher)
    {

        $param = $paramFetcher->all();

        $em = $this->getDoctrine()->getManager();
        $ratings = $em->getRepository('PipedriveDemoBundle:BxBookRating')->getBooksRankingPerLocation($param);

        return $ratings;
    
    }

//- Create REST API for accessing this database using PHP. 
    //In addition to objects CRUD methods which can only be executed by authorized person 
    //create possibility to get books ranking
    // per country publicly (lugejaskond). 
}