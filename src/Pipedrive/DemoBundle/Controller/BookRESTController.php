<?php

namespace Pipedrive\DemoBundle\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\View\View AS FOSView;
use FOS\RestBundle\Util\Codes;

use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations;
use Symfony\Component\Validator\Constraints as Assert;
use Pipedrive\DemoBundle\Entity\BxBook;
use Pipedrive\DemoBundle\Form\BxBookType;

use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface; 

use Pipedrive\DemoBundle\Controller\PipeBaseController;

use Pipedrive\DemoBundle\Controller\TokenAuthenticatedController;

class BookRESTController extends PipeBaseController implements TokenAuthenticatedController
{ 

   /**
    *
    * @Annotations\QueryParam(name="year", nullable=true, description="Year of publication")
    * @Annotations\QueryParam(name="author", nullable=true, description="Author of the book")
    * @Annotations\QueryParam(name="title", nullable=true,description="Title of the book")  
    * @Annotations\QueryParam(name="publisher", nullable=true,description="Publisher of the book")  
    * @Annotations\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing pages.")
    * @Annotations\QueryParam(name="limit", requirements="\d+", default="5", description="How many pages to return.")
    *    
    *       
    * @ApiDoc(
    *  resource=true,
    *  description="Returns a collection of Books by filters",
    *  filters={
    *      {"name"="year", "dataType"="integer", "required"=false, "description"="Year of publication"},
    *      {"name"="author", "dataType"="string", "required"=false, "description"="Author of the book"},    
    *      {"name"="title", "dataType"="string", "required"=false, "description"="Title of the book"},
    *      {"name"="publisher", "dataType"="string", "required"=false, "description"="Publisher"},    
    *  },   
    *  ) 
    *
    * @param ParamFetcher $paramFetcher 
    * @return JsonResponse 
    * 
    */

    public function getBooksAction(ParamFetcher $paramFetcher)
    {

        $param = $paramFetcher->all();

        $em = $this->getDoctrine()->getManager();
        $books = $em->getRepository('PipedriveDemoBundle:BxBook')->getByParam($param);

        return $books;
    }

   /**
    * Get single Book by ISBN
    *
    * @ApiDoc(
    *   resource=true,     
    *   description = "Gets a Book for a given isbn",
    *   output={"class"="BxBook", "groups"={"REST"}},
    *   statusCodes = {
    *     200 = "Returned when successful",
    *     404 = "Returned when the page is not found"
    *   },
    *   parameter={
    *     {"name"="isbn", "dataType"="string", "required"=true, "description"="ISBN code"},
    *   },      
    * )
    *
    * @param Request $request the request object
    *
    * @return array
    *
    * @throws NotFoundHttpException when page not exist
    */

    public function getBookAction($isbn)
    {

        $em = $this->getDoctrine()->getManager();
        $book = $em->getRepository('PipedriveDemoBundle:BxBook')->getOneBookByIsbn($isbn);

        if(!$book) {
            throw $this->createNotFoundException('Book not found!');
        }

        return $book;
    }

   /**
    * Create a new Book entity.
    * @ApiDoc(
    *  resource=true,
    *  description="Create a new Book entity",
    *  output={"class"="BxBook"},
    *  formType="Pipedrive\DemoBundle\Form\BxBookType",
    *  parameters={
    *      {"name"="isbn", "dataType"="string", "required"=true, "description"="ISBN code"},
    *      {"name"="bookTitle", "dataType"="string", "required"=true, "description"="Title of the book"},
    *      {"name"="bookAuthor", "dataType"="string", "required"=true, "description"="Author of the book"},    
    *      {"name"="yearOfPublication", "dataType"="integer", "required"=true, "description"="Year of publication"},     
    *      {"name"="publisher", "dataType"="string", "required"=true, "description"="Publisher of the book"}, 
    *      {"name"="imageUrlS", "dataType"="string", "required"=true, "description"="imageUrlS"},     
    *      {"name"="imageUrlM", "dataType"="string", "required"=true, "description"="imageUrlM"},     
    *      {"name"="imageUrlL", "dataType"="string", "required"=true, "description"="imageUrlL"},     
    *  },
    *   statusCodes = {
    *     201 = "Returned when entity successful created",
    *     400 = "Returned when the form is not valid",
    *   },
    * )     
    *
    * @View(statusCode=201, serializerEnableMaxDepthChecks=true)
    *
    * @param Request $request
    *
    * @return Response
    *
    */

    public function postBookAction(Request $request)
    {
        $entity = new BxBook();
        $form = $this->createForm(new BxBookType(), $entity, array("method" => $request->getMethod()));
        $this->removeExtraFields($request, $form);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $entity;
        } 

        return FOSView::create(array('errors' => $form->getErrors()), Codes::HTTP_BAD_REQUEST);
    }

   /**
    * Update a Book entity
    *
    * @ApiDoc(
    *  resource=true,
    *  description="Update a new Book entity",
    *  output={"class"="BxBook"},
    *  formType="Pipedrive\DemoBundle\Form\BxBookType",
    *  parameters={
    *      {"name"="isbn", "dataType"="string", "required"=true, "description"="ISBN code"},
    *      {"name"="bookTitle", "dataType"="string", "required"=false, "description"="Year of publication"},
    *      {"name"="bookAuthor", "dataType"="string", "required"=false, "description"="Author of the book"},    
    *      {"name"="yearOfPublication", "dataType"="integer", "required"=false, "description"="Title of the book"},     
    *      {"name"="publisher", "dataType"="string", "required"=false, "description"="Publisher of the book"}, 
    *  }      
    * )     
    *
    * @View(statusCode=201, serializerEnableMaxDepthChecks=true)
    *
    * @param Request $request
    * @param string $isbn
    *
    * @return Response
    *
    */

    public function putBookAction(Request $request, $isbn)
    {
        try {

            $em = $this->getDoctrine()->getManager();
            $book = $em->getRepository('PipedriveDemoBundle:BxBook')->find($isbn);

            if(!$book) {
                throw $this->createNotFoundException('Book not found');
            }

            $request->setMethod('PATCH'); //Treat all PUTs as PATCH
            $form = $this->createForm(new BxBookType(), $book, array("method" => $request->getMethod()));
            $this->removeExtraFields($request, $form);
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->flush();

                return $book;
            }

            return FOSView::create(array('errors' => $form->getErrors()), Codes::HTTP_BAD_REQUEST);
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }    

   /**
    * Delete a Book entity.
    *
    * @ApiDoc(
    *  resource=true,
    *  description="Remove Book entity",   
    *  statusCodes = {
    *     204 = "Returned when entity successful removed",
    *   },
    * )      
    *
    * @View(statusCode=204)
    *
    * @param Request $request
    * @param $isbn
    *
    * @return Response
    */

    public function deleteBookAction(Request $request, $isbn)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $book = $em->getRepository('PipedriveDemoBundle:BxBook')->find($isbn);

            if(!$book) {
                throw $this->createNotFoundException('Book not found');
            }

            $em->remove($book);
            $em->flush();

            return null;
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }


  



}
