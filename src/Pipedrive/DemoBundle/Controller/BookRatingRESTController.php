<?php

namespace Pipedrive\DemoBundle\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\View\View AS FOSView;
use FOS\RestBundle\Util\Codes;

use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations;
use Symfony\Component\Validator\Constraints as Assert;
use Pipedrive\DemoBundle\Entity\BxBookRating;
use Pipedrive\DemoBundle\Form\BxBookRatingType;

use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;

use Pipedrive\DemoBundle\Controller\PipeBaseController;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

use JMS\Serializer\Context as SerializationContext;
use Pipedrive\DemoBundle\Controller\TokenAuthenticatedController;


class BookRatingRESTController extends PipeBaseController implements TokenAuthenticatedController

{

   /**
    *
    * @Annotations\QueryParam(name="isbn", nullable=true, description="Isbn")
    * @Annotations\QueryParam(name="user_id", nullable=true, description="userId")    
    * @Annotations\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing pages.")
    * @Annotations\QueryParam(name="limit", requirements="\d+", default="5", description="How many pages to return.")
    *    
    *    
    *       
    * @ApiDoc(
    *  resource=true,
    *  description="Returns a collection of Books by filters",
    *  filters={
    *      {"name"="isbn", "dataType"="string", "required"=false, "description"="Isbn"}, 
    *      {"name"="user_id", "dataType"="string", "required"=false, "description"="userId"},     
    *  },   
    *  ) 
    *
    * @param ParamFetcher $paramFetcher 
    * @return JsonResponse 
    * 
    */

    public function getBookRatingsAction(ParamFetcher $paramFetcher)
    {

        $param = $paramFetcher->all();

        $em = $this->getDoctrine()->getManager();
        $bookRatings = $em->getRepository('PipedriveDemoBundle:BxBookRating')->getBookRatingsByParam($param);

        return $bookRatings;
    }

 
   /**
    * Get single BookRating by Id
    *
    * @ApiDoc(
    *   resource=true,     
    *   description = "Gets a BookRating by Id",
    *   output={"class"="BxBookRating", "groups"={"REST"}},
    *   statusCodes = {
    *     200 = "Returned when successful",
    *     404 = "Returned when the page is not found"
    *   },
    *   parameter={
    *     {"name"="id", "dataType"="integer", "required"=true, "description"="BookRating id"},
    *   },      
    * )
    *
    * @param Request $request the request object
    *
    * @return array
    *
    * @throws NotFoundHttpException when page not exist
    */

    public function getBookRatingAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $bookRating = $em->getRepository('PipedriveDemoBundle:BxBookRating')->getOneById($id);

        if(!$bookRating) {
            throw $this->createNotFoundException('BookRating not found');
        }

        return $bookRating;
    }

   /**
    * Create a new BookRating entity.
    * @ApiDoc(
    *  resource=true,
    *  description="Create a new BookRating entity",
    *  output={"class"="BxBookRating"},
    *  formType="Pipedrive\DemoBundle\Form\BxBookRatingType",
    *  parameters={
    *      {"name"="book", "dataType"="string", "required"=true, "description"="ISBN code"},   
    *      {"name"="user", "dataType"="integer", "required"=true, "description"="User id"},     
    *      {"name"="book_rating", "dataType"="integer", "required"=true, "description"="The Book rating"},  
    *  },
    *   statusCodes = {
    *     201 = "Returned when entity successful created",
    *     400 = "Returned when the form is not valid",
    *   },
    * )     
    *
    * @View(statusCode=201)
    *
    * @param Request $request
    *
    * @return Response
    *
    */

    public function postBookRatingAction(Request $request)
    {
        $entity = new BxBookRating();

        $form = $this->createForm(new BxBookRatingType(), $entity, array("method" => $request->getMethod()));
        $this->removeExtraFields($request, $form);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $em->persist($entity);
            $em->flush();

            return $this->serializeBookRating($entity);

        } 

        return FOSView::create(array('errors' => $form->getErrors()), Codes::HTTP_BAD_REQUEST);
    }

 
  /**
    * Update a BookRating entity
    *
    * @ApiDoc(
    *  resource=true,
    *  description="Update a new BookRating entity",
    *  output={"class"="BxBookRating"},
    *  formType="Pipedrive\DemoBundle\Form\BxBookType",
    *  parameters={
    *      {"name"="book_rating", "dataType"="integer", "required"=true, "description"="The Book rating"},  
    *  }      
    * )     
    *
    * @View(statusCode=201, serializerEnableMaxDepthChecks=true)
    *
    * @param Request $request
    * @param string $id
    *
    * @return Response
    *
    */

    public function putBookRatingAction(Request $request, $id)
    {
        try {

            $em = $this->getDoctrine()->getManager();
            $bookRating = $em->getRepository('PipedriveDemoBundle:BxBookRating')->find($id);

            if(!$bookRating) {
                throw $this->createNotFoundException('BookRating not found');
            }

            $request->setMethod('PATCH'); //Treat all PUTs as PATCH
            $form = $this->createForm(new BxBookRatingType(), $bookRating, array("method" => $request->getMethod()));
            $this->removeExtraFields($request, $form);
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->flush();

                return $this->serializeBookRating($bookRating);
            }

            return FOSView::create(array('errors' => $form->getErrors()), Codes::HTTP_BAD_REQUEST);
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }    

   /**
    * Delete a BookRating entity.
    *
    * @ApiDoc(
    *  resource=true,
    *  description="Remove Book entity",   
    *  statusCodes = {
    *     204 = "Returned when entity successful removed",
    *   },
    * )      
    *
    * @View(statusCode=204)
    *
    * @param Request $request
    * @param $id
    *
    * @return Response
    */

    public function deleteBookRatingAction(Request $request, $id)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $book = $em->getRepository('PipedriveDemoBundle:BxBookRating')->find($id);

            if(!$book) {
                throw $this->createNotFoundException('BookRating not found');
            }

            $em->remove($book);
            $em->flush();

            return null;
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function serializeBookRating(BxBookRating $bookRating) {

        $isbn       = $bookRating->getBook()->getIsbn();
        $userId     = $bookRating->getUser()->getUserId(); 
        $bookRating = $bookRating->getBookRating();

        return array('book' => $isbn , 'user' => $userId, 'book_rating' => $bookRating);

    }

}
