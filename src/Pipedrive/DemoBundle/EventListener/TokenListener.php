<?php
namespace Pipedrive\DemoBundle\EventListener;

use Pipedrive\DemoBundle\Controller\TokenAuthenticatedController;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

use Symfony\Component\HttpFoundation\RequestStack;

class TokenListener
{

    protected $requestStack;
    protected $apiToken;

    public function __construct(RequestStack $requestStack, $apiToken)
    {
        $this->requestStack = $requestStack;
        $this->apiToken = $apiToken;
    }
    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        /*
         * $controller passed can be either a class or a Closure.
         * This is not usual in Symfony but it may happen.
         * If it is a class, it comes in array format
         */

        if (!is_array($controller)) {
            return;
        }


        if ($controller[0] instanceof TokenAuthenticatedController) {
            $requestToken = $event->getRequest()->query->get('api_token');
            if ($this->apiToken !== $requestToken) {
                throw new AccessDeniedHttpException('This action needs a valid token!');
            }
        }
    }

  
}