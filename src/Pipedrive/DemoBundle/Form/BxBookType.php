<?php

namespace Pipedrive\DemoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BxBookType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('isbn')
            ->add('bookTitle')
            ->add('bookAuthor')
            ->add('yearOfPublication')
            ->add('publisher')
            ->add('imageUrlS')
            ->add('imageUrlM')
            ->add('imageUrlL')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Pipedrive\DemoBundle\Entity\BxBook',
            'csrf_protection'   => false
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'pipedrive_demobundle_bxbook';
    }
}
