<?php
namespace Pipedrive\DemoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


use Doctrine\ORM\EntityRepository;



class BxBookRatingType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
             ->add('book_rating')         
             ->add('book', 'entity', array(
                                            'class' => 'Pipedrive\DemoBundle\Entity\BxBook',                                            
                                            'property' => 'isbn',
                                            'query_builder' => function(EntityRepository $er) {
                                                     return $er->createQueryBuilder('p')->setMaxResults(1);
                                             }
                 ))           
             ->add('user', 'entity', array(
                                            'class' => 'Pipedrive\DemoBundle\Entity\BxUser',                                            
                                            'property' => 'user_id',
                                            'query_builder' => function(EntityRepository $er) {
                                                     return $er->createQueryBuilder('p')->setMaxResults(1);
                                             }
                 ))               
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Pipedrive\DemoBundle\Entity\BxBookRating',
            'csrf_protection'   => false
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'pipedrive_demobundle_bxbookrating';
    }
}
